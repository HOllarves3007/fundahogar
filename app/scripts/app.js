'use strict';

/**
 * @ngdoc overview
 * @name fundaHogarSiteApp
 * @description
 * # fundaHogarSiteApp
 *
 * Main module of the application.
 */
var fundaHogarSiteApp = angular
  .module('fundaHogarSiteApp', [
    'ngRoute',
    'ngTouch',
    'youtube-embed'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });


fundaHogarSiteApp.controller('mainVideoController',function($scope){
    $scope.mainVideo = 'https://www.youtube.com/watch?v=XQu8TTBmGhA';
    console.log("mainVideoController");
});