'use strict';

/**
 * @ngdoc function
 * @name fundaHogarSiteApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the fundaHogarSiteApp
 */
angular.module('fundaHogarSiteApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
