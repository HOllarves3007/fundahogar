'use strict';

/**
 * @ngdoc function
 * @name fundaHogarSiteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fundaHogarSiteApp
 */
angular.module('fundaHogarSiteApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
